package de.patterns.templatemethod;

import java.util.LinkedList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class GesamtTabelleTest {

	@Test
	public void testBerechnePunkte(){
		Tabelle tabelle = new GesamtTabelle();
		List<Spiel> spiele = new LinkedList<>();
		//Pfad 1: zwei Unbekannte Mannschaften, Heim gewinnt gegen Gast
		Spiel spiel1 = new Spiel(1,2,0,"Mannschaft1","Mannschaft2");
		//Pfad 2: zwei Bekannte Mannschaften, Unentschieden
		Spiel spiel2 = new Spiel(2,0,0,"Mannschaft1","Mannschaft2");
		//Pfad 3: zwei Bekannte Mannschaften, Gast gewinnt
		Spiel spiel3 = new Spiel(3,0,1,"Mannschaft1","Mannschaft2");
		//Pfad 4: Platzhalter, um weniger Spieltage zu berechnen -> Bedingung unter dem for(Spiel spiel: spiele)
		Spiel spiel4 = new Spiel(4,0,0,"Mannschaft1","Mannschaft2");
		spiele.add(spiel1);
		spiele.add(spiel2);
		spiele.add(spiel3);
		spiele.add(spiel4);
		List<Mannschaft> ergebnis = tabelle.berechnePunkte(spiele, 3);
		//Assertions auf die Ergebnisse, um die Berechnungen zu pr�fen
		Assertions.assertTrue(ergebnis.size() == 2);
		Mannschaft mannschaft1 = null;
		Mannschaft mannschaft2 = null;
		for(Mannschaft currentMannschaft: ergebnis) {
			if("Mannschaft1".equals(currentMannschaft.getName())) {
				mannschaft1 = currentMannschaft;
			}
			else {
				mannschaft2 = currentMannschaft;
			}
		}
		Assertions.assertNotNull(mannschaft1);
		Assertions.assertNotNull(mannschaft2);
		Assertions.assertEquals(2, mannschaft1.getTore());
		Assertions.assertEquals(1, mannschaft2.getTore());
		Assertions.assertEquals(1, mannschaft1.getGegentore());
		Assertions.assertEquals(2, mannschaft2.getGegentore());
		Assertions.assertEquals(4, mannschaft1.getPunkte());
		Assertions.assertEquals(4, mannschaft2.getPunkte());
	}
}
