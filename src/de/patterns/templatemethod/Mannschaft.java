package de.patterns.templatemethod;

public class Mannschaft implements Comparable<Mannschaft> {
	private String name;
	private int punkte;
	private int tore;
	private int gegentore;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPunkte() {
		return punkte;
	}

	public void setPunkte(int punkte) {
		this.punkte = punkte;
	}

	public int getTore() {
		return tore;
	}

	public void setTore(int tore) {
		this.tore = tore;
	}

	public int getGegentore() {
		return gegentore;
	}

	public void setGegentore(int gegentore) {
		this.gegentore = gegentore;
	}

	@Override
	public int compareTo(Mannschaft o) {
		if (punkte > o.getPunkte()) {
			return -1;
		} else if (punkte < o.getPunkte()) {
			return 1;
		} else {
			if ((tore - gegentore) > (o.getTore() - o.getGegentore())) {
				return -1;
			} else if ((tore - gegentore) < (o.getTore() - o.getGegentore())) {
				return 1;
			} else {
				// Hier koennten weitere Vergleiche angestellt werden, bevor die Objekte als
				// gleich gelten.
				return 0;
			}
		}
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Mannschaft) {
			Mannschaft other = (Mannschaft) o;
			if (other.getName().equals(name)) {
				return true;
			}
		}
		return false;
	}

}
