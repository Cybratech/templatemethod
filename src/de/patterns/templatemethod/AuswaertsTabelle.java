package de.patterns.templatemethod;

import java.util.LinkedList;
import java.util.List;

public class AuswaertsTabelle extends Tabelle {

	@Override
	public List<Mannschaft> berechnePunkte(List<Spiel> spiele, int bisSpieltag) {
		List<Mannschaft> tabelle = new LinkedList<Mannschaft>();
		for (Spiel spiel : spiele) {
			if (spiel.getSpieltag() <= bisSpieltag) {
				Mannschaft gast = new Mannschaft();
				Mannschaft heim = new Mannschaft();
				heim.setName(spiel.getNameHeim());
				gast.setName(spiel.getNameGast());
				for (Mannschaft mannschaft : tabelle) {
					if (mannschaft.equals(gast)) {
						gast = mannschaft;
					}
				}
				gast.setTore(spiel.getToreGast() + gast.getTore());
				gast.setGegentore(spiel.getToreHeim() + gast.getGegentore());
				if (spiel.getToreHeim() < spiel.getToreGast()) {
					gast.setPunkte(gast.getPunkte() + 3);
				} else if (spiel.getToreHeim() == spiel.getToreGast()) {
					gast.setPunkte(gast.getPunkte() + 1);
				}
				if (!tabelle.contains(gast)) {
					tabelle.add(gast);
				}
				if (!tabelle.contains(heim)) {
					tabelle.add(heim);
				}
			}
		}
		return tabelle;
	}

}
