package de.patterns.templatemethod;

import java.util.LinkedList;
import java.util.List;

public class GesamtTabelle extends Tabelle {

	@Override
	public List<Mannschaft> berechnePunkte(List<Spiel> spiele, int bisSpieltag) {
		List<Mannschaft> tabelle = new LinkedList<Mannschaft>();
		for (Spiel spiel : spiele) {
			if (spiel.getSpieltag() <= bisSpieltag) {
				Mannschaft heim = new Mannschaft();
				heim.setName(spiel.getNameHeim());
				Mannschaft gast = new Mannschaft();
				gast.setName(spiel.getNameGast());
				for (Mannschaft mannschaft : tabelle) {
					if (mannschaft.equals(heim)) {
						heim = mannschaft;
					}
				}
				for (Mannschaft mannschaft : tabelle) {
					if (mannschaft.equals(gast)) {
						gast = mannschaft;
					}
				}
				heim.setTore(spiel.getToreHeim() + heim.getTore());
				heim.setGegentore(spiel.getToreGast() + heim.getGegentore());
				gast.setTore(spiel.getToreGast() + gast.getTore());
				gast.setGegentore(spiel.getToreHeim()+gast.getGegentore());
				if (spiel.getToreHeim() > spiel.getToreGast()) {
					heim.setPunkte(heim.getPunkte() + 3);
				} else if (spiel.getToreHeim() < spiel.getToreGast()) {
					gast.setPunkte(gast.getPunkte() + 3);
				} else {
					heim.setPunkte(heim.getPunkte() + 1);
					gast.setPunkte(gast.getPunkte() + 1);
				}
				if (!tabelle.contains(heim)) {
					tabelle.add(heim);

				}
				if (!tabelle.contains(gast)) {
					tabelle.add(gast);
				}
			}
		}
		return tabelle;
	}

	/**
	 * Ueberschreiben der Hook-Methode.
	 */
	@Override
	public boolean faerbeEin() {
		return true;
	}

}
