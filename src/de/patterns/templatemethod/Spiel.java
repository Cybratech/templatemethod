package de.patterns.templatemethod;

public class Spiel {
	private int spieltag;
	private int toreHeim;
	private int toreGast;
	private String nameHeim;
	private String nameGast;

	public Spiel(int spieltag, int toreHeim, int toreGast, String nameHeim, String nameGast) {
		this.spieltag = spieltag;
		this.toreHeim = toreHeim;
		this.toreGast = toreGast;
		this.nameGast = nameGast;
		this.nameHeim = nameHeim;
	}

	public Spiel() {

	}

	public int getSpieltag() {
		return spieltag;
	}

	public void setSpieltag(int spieltag) {
		this.spieltag = spieltag;
	}

	public int getToreHeim() {
		return toreHeim;
	}

	public void setToreHeim(int toreHeim) {
		this.toreHeim = toreHeim;
	}

	public int getToreGast() {
		return toreGast;
	}

	public void setToreGast(int toreGast) {
		this.toreGast = toreGast;
	}

	public String getNameHeim() {
		return nameHeim;
	}

	public void setNameHeim(String nameHeim) {
		this.nameHeim = nameHeim;
	}

	public String getNameGast() {
		return nameGast;
	}

	public void setNameGast(String nameGast) {
		this.nameGast = nameGast;
	}

}
