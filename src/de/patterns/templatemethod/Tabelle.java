package de.patterns.templatemethod;

import java.util.Collections;
import java.util.List;

public abstract class Tabelle {

	public void erstelleTabelle(List<Spiel> spiele, int bisSpieltag) {
		List<Mannschaft> mannschaften = berechnePunkte(spiele, bisSpieltag);
		Collections.sort(mannschaften);

		StringBuilder builder = new StringBuilder();
		builder.append("Rang").append("\t");
		builder.append("Name").append("\t").append("\t").append("\t").append("\t");
		builder.append("Punkte").append("\t");
		builder.append("Tore").append("\t");
		builder.append("Gegentore");
		if (faerbeEin()) {
			builder.append("\t").append("Farbe");
		}
		builder.append("\n");
		for (int i = 0; i < mannschaften.size(); i++) {
			Mannschaft mannschaft = mannschaften.get(i);
			builder.append((i + 1) + ".").append("\t");
			builder.append(namenAuffuellen(mannschaft.getName())).append("\t");
			builder.append(mannschaft.getPunkte()).append("\t");
			builder.append(mannschaft.getTore()).append("\t");
			builder.append(mannschaft.getGegentore()).append("\t");
			if (faerbeEin()) {
				builder.append("\t").append(getFarbeForPlatz(i + 1));
			}
			builder.append("\n");
		}
		System.out.println(builder.toString());
	}

	/**
	 * Konkrete Methode fuer jede Tabelle.
	 * 
	 * @param spiele
	 * @param bisSpieltag
	 * @return
	 */
	public abstract List<Mannschaft> berechnePunkte(List<Spiel> spiele, int bisSpieltag);

	/**
	 * Hook zum einfaerben.
	 * 
	 * @return true, wenn die Tabelle eingefaerbt werden soll.
	 */
	public boolean faerbeEin() {
		return false;
	}

	/**
	 * Hilfsmethode zum bestimmten der Farbe fuer eine Platzierung
	 * 
	 * @param platzierung
	 * @return leerer String bei keiner Farbe, sonst die Farbe
	 */
	private String getFarbeForPlatz(int platzierung) {
		if (platzierung <= 4) {
			return "{blau}";
		} else if (platzierung > 4 && platzierung <= 6) {
			return "{gruen}";
		} else if (platzierung >= 16) {
			return "{rot}";
		} else {
			return "";
		}
	}

	/**
	 * Hilfsmethode zum auffuellen der Strings damit die Tabelle gleich aussieht.
	 * 
	 * @param name der name der Mannschaft
	 * @return name auf eine laenge mit 25 Zeichen
	 */
	private String namenAuffuellen(String name) {
		return String.format("%-25s", name);
	}

}
