package de.patterns.templatemethod;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Client {
	private static final String COMMA_DELIMITER = ";";

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Geben Sie den Pfad zur CSV Datei ein (absolout): ");
		String pfad = br.readLine();
		System.out.println("Geben Sie den gewuenschten Spieltag an (1-29):");
		String spieltagString = br.readLine();
		int spieltag = Integer.parseInt(spieltagString);
		System.out.println("Geben sie Die Tabellenart an (Gesamt/Heim/Auswaerts): ");
		String art = br.readLine();
		Tabelle tabelle = null;
		switch (art.toUpperCase()) {
		case "GESAMT":
			tabelle = new GesamtTabelle();
			break;
		case "HEIM":
			tabelle = new HeimTabelle();
			break;
		case "AUSWAERTS":
			tabelle = new AuswaertsTabelle();
			break;
		default:
			break;
		}
		if(tabelle!=null) {
			tabelle.erstelleTabelle(leseMannschaftenEin(pfad), spieltag);
		}
		else {
			System.out.println("Keine Valide Tabellenart");
		}
		
	}
	public static List<Spiel> leseMannschaftenEin(String pfad) throws FileNotFoundException, IOException {
		List<Spiel> spiele = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(pfad))) {
			String line;
			br.readLine();
			while ((line = br.readLine()) != null) {
				String[] values = line.split(COMMA_DELIMITER);
				Spiel spiel = new Spiel();
				spiel.setSpieltag(Integer.parseInt(values[0]));
				spiel.setNameHeim(values[1]);
				spiel.setNameGast(values[2]);
				spiel.setToreHeim(Integer.parseInt(values[3]));
				spiel.setToreGast(Integer.parseInt(values[4]));
				spiele.add(spiel);
			}
			return spiele;
		}
	}
}
